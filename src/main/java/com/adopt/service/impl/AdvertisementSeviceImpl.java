package com.adopt.service.impl;

import com.adopt.model.Advertisement;
import com.adopt.repository.AdvertisementRepository;
import com.adopt.repository.entity.AdvertisementEntity;
import com.adopt.service.AdvertisementMapper;
import com.adopt.service.AdvertisementService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.adopt.service.AdvertisementMapper.fromEntity;

@Service
public class AdvertisementSeviceImpl implements AdvertisementService {

    private final AdvertisementRepository repository;

    AdvertisementSeviceImpl(AdvertisementRepository repository) {
        this.repository = repository;
    }

    @Override
    public Advertisement createAdvertisement(String description) {
        var entity = repository.save(new AdvertisementEntity().setDescription(description));
        return fromEntity(entity);
    }

    @Override
    public List<Advertisement> getAds() {
        return repository.findAll().stream()
                .map(AdvertisementMapper::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Advertisement> getAd(String id) {
        var entity = repository.findById(id);
        return entity.map(AdvertisementMapper::fromEntity);
    }
}
