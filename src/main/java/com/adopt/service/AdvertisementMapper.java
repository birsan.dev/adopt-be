package com.adopt.service;

import com.adopt.model.Advertisement;
import com.adopt.repository.entity.AdvertisementEntity;

public class AdvertisementMapper {

    public static Advertisement fromEntity(AdvertisementEntity entity) {
        return new Advertisement(entity.getId(), entity.getDescription());
    }

    public static AdvertisementEntity toEntity(Advertisement advertisement) {
        return new AdvertisementEntity()
                .setId(advertisement.getId())
                .setDescription(advertisement.getDescription());
    }
}
