package com.adopt.service;

import com.adopt.model.Advertisement;

import java.util.List;
import java.util.Optional;

public interface AdvertisementService {

    Advertisement createAdvertisement(String description);

    List<Advertisement> getAds();

    Optional<Advertisement> getAd(String id);
}
