package com.adopt;

import com.adopt.repository.AdvertisementRepository;
import com.adopt.repository.entity.AdvertisementEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    private final AdvertisementRepository advertisementRepository;

    @Autowired
    public Application(AdvertisementRepository advertisementRepository) {
        this.advertisementRepository = advertisementRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> advertisementRepository.save(new AdvertisementEntity().setDescription("dummy description"));
    }
}
