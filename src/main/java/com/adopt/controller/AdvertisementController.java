package com.adopt.controller;

import com.adopt.model.Advertisement;
import com.adopt.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/adopt")
public class AdvertisementController {

    private final AdvertisementService adService;

    @Autowired
    AdvertisementController(AdvertisementService adService) {
        this.adService = adService;
    }

    @GetMapping
    public String welcome() {
        return "Welcome to our application!";
    }

    @GetMapping("/ads")
    public List<Advertisement> getAds() {
        return adService.getAds();
    }

    @GetMapping("/ad")
    public Advertisement getAd(@RequestParam("id") String id) {
        return adService.getAd(id).orElse(null);
    }
}
