package com.adopt.model;

public class Advertisement {

    private String id;
    private String description;

    public Advertisement(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
