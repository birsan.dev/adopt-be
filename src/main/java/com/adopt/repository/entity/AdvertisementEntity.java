package com.adopt.repository.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class AdvertisementEntity {

    @Id
    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public AdvertisementEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AdvertisementEntity setDescription(String description) {
        this.description = description;
        return this;
    }
}
