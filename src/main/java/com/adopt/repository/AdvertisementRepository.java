package com.adopt.repository;

import com.adopt.repository.entity.AdvertisementEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertisementRepository extends MongoRepository<AdvertisementEntity, String> {
}
